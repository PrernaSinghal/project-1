// Job of the class
class Rectangle {

    private final int length;
    private final int breadth;

    Rectangle(int length,int breadth){
        this.length = length;
        this.breadth = breadth;
    }

    private boolean validateRectangle(int length, int breadth) {
        if(length<=0 || breadth<=0) return false;
        return true;
    }

    int calculateArea() throws NotValidInputException{
        if(!validateRectangle(length,breadth)){
            throw new NotValidInputException();
        }
        return length*breadth;
    }

    int calculatePerimeter() throws NotValidInputException {
        if(!validateRectangle(length,breadth)){
            throw new NotValidInputException();
        }
        return 2*(length+breadth);
    }
}
