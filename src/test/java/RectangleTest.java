import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RectangleTest {
    private Rectangle rectangle;

    @Test
    public void shouldReturnArea12WhenLengthAndBreadthHas4And3() {
        rectangle = new Rectangle(4, 3);
        try{
            assertEquals(12, rectangle.calculateArea());
        }catch(NotValidInputException e){
            Assert.fail("Error");
        }
    }

    @Test
    public void shouldReturnAreaMinus1WhenLengthAndBreadthHasMinusValues() {
        rectangle = new Rectangle(-4, 1);
        try{
            int area =  rectangle.calculateArea();
            Assert.fail("Error");
        }catch(NotValidInputException e){
            assertEquals("Invalid Input", e.getMessage());
        }
    }

    @Test
    public void shouldReturnAreaMinus1WhenLengthAndBreadthIsZero() {
        rectangle = new Rectangle(4, 0);
        try{
            int area = rectangle.calculateArea();
            Assert.fail("Error");
        }catch(NotValidInputException e){
            assertEquals("Invalid Input", e.getMessage());
        }
    }

    @Test
    public void shouldReturnPerimeter14WhenLengthAndBreadthIs4And3() {
        rectangle = new Rectangle(4, 3);
        try {
            int perimeter = rectangle.calculatePerimeter();
            assertEquals(14, perimeter);
        }catch(NotValidInputException e){
            Assert.fail("Error");
        }
    }

    @Test
    public void shouldReturnPerimeterMinus1WhenLengthOrBreadthIsNegative() {
        rectangle = new Rectangle(-2, 4);
        try {
            int perimeter = rectangle.calculatePerimeter();
            Assert.fail("Error");
        }catch(NotValidInputException e){
            assertEquals("Invalid Input", e.getMessage());
        }
    }

    public void shouldReturnAreaOfSquare16WhenLength4(){
        rectangle =new Rectangle(4,4);
        try {
            assertEquals(16, rectangle.calculateArea());
        }catch(NotValidInputException e){
            Assert.fail("Error");
        }
    }

}
