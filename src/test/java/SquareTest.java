import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SquareTest {
    private Square square;

    @Test
    public void shouldReturnArea16WhenLengthIs4(){
        square = new Square(4);
        try {
            assertEquals(16, square.calculateArea());
        }catch(NotValidInputException e){
        }
    }

    @Test
    public void shouldReturnAreaMinus1WhenLengthAndBreadthHasMinusValues(){
        square = new Square(4);
        try {
            int area =  square.calculateArea();
        }catch(NotValidInputException e){
            assertEquals("Invalid Input",e.getMessage());
        }
    }


}
